# Functional Analysis

## Foreword

These are some notes I am making on Functional Analysis.
The point is to gather all the materials necessary to understand the basic mathematical problems of quantum field theory.
I might delve a bit into applications in integral equations and partial differential equations, but I want to keep these abstract.
Topics I need to touch on:

1) Banach spaces

    a) Hahn-Banach theorem

    b) Principle of Uniform Boundedness

    c) Open Mapping Theorem

    d) Closed Graph Theorem

2) Hilbert spaces

   a) Riesz Representation Theorem

   b) Lax-Milgram

   c) Orthonormal Bases

3) Theory of Distributions

4) Microlocal analysis?

The list above is not exhaustive, but is merely a starting point for an outline of my notes.
I would like to follow Stein and Shakarchi *Functional Analysis*.
Other texts may be added as references as needed.
For example, Stein and Shakarchi does not address pseudo-differential operators of microlocal analysis, so we may need to look into using Hormander or Taylor for this topic.
*Real Analysis* and *Partial Differential Equations* by Folland cover various aspects of the topics I am interested in, so we'll look into those as well.
In fact, let's change it since Folland's monograph on quantum field theory is what I want to understand.

## Basics of Functional Analysis

The primary objects studied in Functional Analysis are (abstract) normed vector spaces and the linear maps between them with a particular focus on infinite-dimensional vector spaces.
Thus, Functional Analysis may be viewed as the generalization of linear algebra to infinite dimensional vector spaces, or, that linear algebra is the finite-dimensional restriction of functional analysis.
The generalization to infinite dimensions raises new questions about the notion of convergence.
These questions are not present in the theory of finite dimensional normed vector spaces since all norms define equivalent topologies in finite dimensional settings.
Most of the generalization occurs on the topological side, while the algebraic study of infinite dimensional normed spaces is not really any different from the finite-dimensional case.

### Normed vector spaces

Let $X$ be a vector space over $K$ where $K = \mathbb{R}$ or $\mathbb{C}$.
The zero vector of $X$ and the zero element of $K$ are both denoted $0$ where the meaning of $0$ should be clear from context.
A subspace will always refer to a vector subspace.
For $x \in X$, $Kx$ denotes the one-dimensional vector subspace spanned by $x$.
If $M,N$ are subspaces of $X$, then $M + N$ denotes the subspace $\{x + y : x \in M, y \in N\}$.

A **seminorm** on $X$ is a function $\| \cdot \| : X \to [0, \infty)$ such that:

- $\|x + y\| \leq \|x\| + \|y\|$;
  
- $\| \lambda x \| = | \lambda | \| x \|$ for all $\lambda \in K, x \in X$.

If $\| \cdot \|$ is a seminorm such that $\| x \| = 0 \Rightarrow x = 0$, then we call $\| \cdot \|$ a norm.
If $\| \cdot \|$ is a norm on $X$, we call the pair $(X, \| \cdot \|)$ a **normed vector space** or **normed linear space**.

The reader may directly verify that if $\| \cdot \|$ is a norm on $X$, then $d(x,y) := \|x - y\|$ makes $d$ a metric on $X$.
Therefore every normed vector space is a metric space, and consequently also a topological space.
The topology defined by the metric induced by the norm $\| \cdot \|$ is called the **norm topology**.

Two norms $\| \cdot \|_1, \| \cdot \|_2$ on $X$ called **equivalent** if there exists $c_1, c_2 > 0$ such that

$$
c_1 \| x \|_1 \leq \| x \|_2 \leq c_2 \| x \|_1 \quad \forall x \in X.
$$

Equivalent norms on $X$ define equivalent metrics, so equivalent norms define the same topologies on $X$.

A normed vector space that is complete with respect to the metric induced by the norm is called a **Banach space**.
We will see that every normed vector space may be embedded in a Banach space as a dense subset.
In a sense, every normed vector space is close to a Banach space.
One way to do this is by using Cauchy sequences similar to how $\mathbb{R}$ may be contructed from $\mathbb{Q}$.
An alternative embedding will be demonstrated later.

An **infinite seres** $\sum_{j=1}^\infty x_n$ of vectors $\{x_j\}_{j \in \mathbb{N}}$ in $X$ is the sequence of partial sums $S_N,  N \in \mathbb{N}$ with

$$
S_N := \sum_{j=1}^N x_n.
$$
We say a infinite series **converges** if there is $x \in X$ such that $\|S_N - x\| \to 0$ as $N \to \infty$, and we say that the infinite series **converges absolutely** if the series of non-negative numbers $\sum_{j=1}^\infty \|x_j\|$ converges.

**Theorem**
A normed vector space is complete if and only if every absolutely convergent series is convergent.

**Proof:**
$(\Rightarrow)$ Let $X$ be a Banach space and $\sum_{j=1}^\infty x_j$ be an absolutely convergent series.
Let $n, m \in \mathbb{N}, m < n$.
We may apply the triangle inequality to get

$$
\left\| \sum_{j=1}^n x_j - \sum_{j=1}^m x_j \right\| \leq \sum_{j=m}^n \|x_j\|.
$$

$\sum_{j=1}^\infty x_j$ is absolutly convergent, so the RHS tends to $0$ as $m,n \to \infty$, therefore the sequence of partial sums of $\sum_j x_j$ form a Cauchy sequence in $X$.
This sequence is convergent because $X$ is complete.

$(\Leftarrow)$ Let $X$ be a normed vector space where every absolutely convergent series is convergent, and let $\{x_j\}$ be a Cauchy sequence in $X$.
Then we can choose an increasing sequence $n_1,n_2,n_3,\ldots$  such that $\|x_n - x_m \| < 2^{-j}$ for $n,m > n_j$.
Let $y_1 = x_{n_1}$ and $y_{n_j} = x_{n_j} - x_{n_{j-1}}$ for all other $j$.
Then $\sum_{j=1}^k y_j = x_{n_k}$ and

$$
\sum_1^\infty \|y_j\| \leq \|y_1\| + \sum_j^\infty 2^{-j} = \|y_1\| + 1 < \infty
$$

so $\sum_j y_j$ is absolutely convergent, and hence convergent.
This implies that $x_{n_j}$ is a convergent subsequence of a Cauchy subsequene.
It remains to show that $x_j$ is itself convergent.
Let $\varepsilon > 0$.
$x_n$ is Cauchy so there is $N_1 \in \mathbb{N}$ such that $\|x_n - x_m\| < \varepsilon/2$ for all $n,m > N_1$.
$x_{n_j}$ is convergent so there is $N_2 \in \mathbb{N}$ such that $\|x_{n_j} - x\| < \varepsilon/2$ for all $n_j > N_2$.
Set $N = \max(N_1,N_2)$ and choose any $n_j > N_1$.
Then if $n > N$ we have

$$
\|x_n - x\| \leq \|x_n - x_{n_j}\| + \|x_{n_j} - x\| < \frac{\varepsilon}{2} + \frac{\varepsilon}{2}= \varepsilon.
$$

This shows that $X$ is complete. $\blacksquare$

**Examples**

(1) $\mathbb{R}^n$ is a Banach space.

(2) Let $X$ be a topological space.
Then the set of all bounded complex valued functions $B(X)$ and the set of all bounded continuous complex valued functions $BC(X)$ are Banach spaces with respect to the uniform norm $\| f \|_u := \sup_{x \in X} |f(x)|$.

(3) Let $(X, \mathcal{M}, \mu)$ be a measure space.
Then $L^1(\mu)$ is a Banach space.
Recall that $L^1(\mu)$ consists of *equivalence classes* of functions, rather than actual functions on $X$.
$\int |f| \: d\mu$ only defines a seminorm on the actual functions.

Given two normed vector spaces $X,Y$, the Cartesian product $X \times Y$ becomes a normed vector space when equipped with the **product norm**

$$
\| (x,y) \| := \max(\| x \|, \| y \|)
$$

For any $1 \leq p < \infty$, $(\|x\|^p + \|y\|^p)^{1/p}$ defines an equivalent norm to $\max(\| x \|, \| y \|)$ on $X \times Y$.

If $M$ is a subspace of the normed vector space $X$, we may define an equivalence relation on $X$ using $M$.
We say $x \sim y$ if $x - y \in M$.
The equivalent class of $x$ is denoted $x + M$, and the set of equivalence classes is called a **quotient space**, and is denoted $X/M$.
$X/M$ inherits the **qoutient norm**

$$
\| x + M \| := \inf_{y \in M} \|x + y\|.
$$

Now we discuss some properties of linear maps on normed vector spaces, the first of which is continuity.
Linear maps between finite dimensional vector spaces are trivailly contiuous, however, this does not hold in the abstract infinite dimensional setting.
A linear map $T:X \to Y$ between nored vector spaces is called **bounded** if there exists $C \geq 0$ such that

$$
\|Tx\| \leq C\|x\| \quad \forall x \in X.
$$

This says that the linear map $T$ is bounded if the image of a bounded subset of $X$ is also bounded in $Y$, which is weaker than the ordinary notion of boundedness for functions.
The range of any nonzero linear map must be unbounded.

**Proposition:** Let $X, Y$ be normed vector spaces, and $T:X \to Y$ be a linear map.
TFAE:

(1) $T$ is continuous

(2) $T$ is continuous at $0$

(3) $T$ is bounded

Before proceeding to the proof, we note that this result is trivial in finite dimensions where all linear maps are bounded and continuous.

**Proof:** (1) implies (2) is trivial.
If $T$ is continuous at $0$, then there is a ball $B_\delta(0)$ such that $x \in B_\delta(0) \Rightarrow \| Tx - T(0)\| \leq 1$ so $\| Tx \| \leq 1$ for $\| x \| \leq \delta$.
Then it follows that $\| Tx \| \leq \delta^{-1}\| x \|$ for all $x$, so $T$ is bounded.
Now if $T$ is bounded, then there is $C \geq 0$ such that

$$
\| Tx - Ty \| = \| T(x-y) \| \leq C \|x - y\|
$$

which shows that $T$ is Lipschitz continuous. $\blacksquare$

Given two normed vector spaces $X,Y$, the vector space of all bounded linear maps from $X \to Y$ is denoted $L(X,Y)$.
$L(X,Y)$ is a normed vector space with the norm defined by

$$
\| T \| := \sup\{\| Tx \| : \|x\| = 1\} = \sup\left\{ \frac{\| Tx \|}{\| x \|} : x \neq 0 \right\} = \inf\{C : \| Tx \| \leq C\|x\| \forall x\}.
$$

This is norm is called the **operator norm**, and we always assume that $L(X,Y)$ carries this norm unless specified otherwise.
The normability of $L(X,Y)$ presumes that both $X,Y$ carry norms themselves, however, the completeness of $L(X,Y)$ requires only $Y$ to be a Banach space.

**Theorem** If $Y$ is Banach space, then so is $L(X,Y)$.

**Proof:** Let $T_n$ be a Cauchy sequence in $L(X,Y)$.
Then if $x \in X$, $\|T_n x - T_m x \| \leq \| T_n - T_m \| \|x\|$ so $T_n x$ is Cauchy in $Y$.
$Y$ is complete so there is $y \in Y$ such that $T_n x \to y$ as $n \to \infty$.
Thus we may construct a map $T : X \to Y$ defined by a pointwise limit $Tx := \lim_{n \to \infty} T_n x$.
$T$ is linear because pointwise limits of vectors in Banach spaces are linear.

It remains to show that $T$ is bounded.
Note that $| \|T_n\| - \|T_m\| | \leq \| T_n - T_m \|$, so $\|T_n\|$ is a Cauchy sequence in $\R$.
Then there is $L \geq 0$ such that $\|T_n\| \to L$ in $\R$.
We have

$$
\| Tx \| = \left\| \lim_n T_n x \right\| = \lim_n \| T_n x \| \leq \lim_n \|T_n\| \|x\| = L\|x\|
$$

therefore $T$ is bounded, completing the proof. $\blacksquare$

The norm on a Banach space is continuous, so using the notation of the preceding proof we have


**Corollary** If $T_n \to T$ in $L(X,Y)$, then $\|T_n\| \to \|T\|$.

**Proof:** This follows from the continuity of the norm on Banach spaces.

If $T \in L(X,Y)$ and $S \in L(Y,Z)$ then the composition $ST : X \to Z$ satisfies

$$
\|STx\| \leq \|S\|\|Tx\| \leq \|S\|\|T\|\|x\|.
$$

Therefore $ST \in L(X,Z)$ with operator $\|ST\| \leq \|S\|\|T\|$.
In particular, $L(X,X)$ is an algebra.
When $X$ is complete, then $L(X,X)$ is a **Banach algebra**, i.e., a Banach space that is also an algebra where the norm of a product is at most the product of the norms.
More generally, if $X$ is a topological space, then $BC(X)$ is a Banach algebra with pointwise multiplication and the uniform norm.

If $T \in L(X,Y)$, we say $T$ an isometry if $\|Tx\| = \|x\|$ for all $x \in X$.
We say $T$ is **invertible** or an **isomorphism** if $T$ is bijective with continuous inverse.
Note that isometries are injective, but not necessarily isomorphisms onto all of $Y$.
Isometries are isomorphisms onto their range.